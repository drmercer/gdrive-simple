# gdrive-simple
An intuitive and object-oriented wrapper on the Google Drive API. Exposes File and Folder objects which have Promise-based asynchronous functions. Also includes a simple utility for managing signed-in state.

Check out the source code [here](https://gitlab.com/drmercer/gdrive-simple/tree/master/src).

# Example usage
```javascript
import gds from "gdrive-simple";

gds.init({
	clientId: "YOUR CLIENT ID",
	scopes: [
		// Use this scope if you want access to your private app data:
		"https://www.googleapis.com/auth/drive.appdata",
		// This one lets you access files you created in the user's
		// Drive (i.e. not in your app's data folder):
		"https://www.googleapis.com/auth/drive.file",
		// This one gives you full access (e.g. to files you didn't
		// create):
		"https://www.googleapis.com/auth/drive",
		// For all scopes go here:
		// https://developers.google.com/drive/v3/web/about-auth
	],
}).then(() => {
	// The listener given here will be called promptly with the
	// current signed-in state, and again whenever that state changes.
	gds.listenForSignInChange(handleSignedInChange);
});

function handleSignedInChange(isSignedIn) {
	if (!isSignedIn) {
		// Show some "Sign in button", call gds.signIn() when that
		// button is clicked
		document.getElementById("signIn").onclick = gds.signIn;
	} else {
		// Do whatever you want! Examples:

		// Get the user's Drive root:
		const root = gds.getRootFolder();
		// Get your app's private App Data folder:
		const appdata = gds.getAppDataFolder();

		// List files in a folder:
		root.listFiles().then(files => {
			console.log("Files in the user's Drive:", files);
		});

		// Get a file by name (creating it if it doesn't exist), and
		// write some JSON to it:
		appdata.getFile("myfile.json")
			.then(file => file.write( {thisIsSome:"json"} ))
			.then(() => console.log("myfile.json succesfully written."));

		// Get a subdirectory, then get a file inside it:
		appdata.getFolder("myfolder")
			.then(folder => folder.getFile("myfileinmyfolder.json"))
			.then(file => file.read())
			.then(contents => console.log("Contents:", contents));
	}
}
```

# License
`gdrive-simple` is licensed under the MIT License. However, Google's JavaScript client library, which this library depends upon, is licensed under an Apache License, so please take care to also comply with the terms for that library.
