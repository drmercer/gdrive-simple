/* global gapi */

const FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
// The maximum page size supported by drive.file.list:
const MAX_PAGE_SIZE = 1000;

export default class NetworkUtils {

	//======================================================================
	//			Used by Folder class

	listFolderChildren(id, spaces, {folders=false, getAll=false, pageSize=10}={}) {
		const requestParams = {
			q:
				`'${id}' in parents `+
				`and mimeType ${folders?"=":"!="} '${FOLDER_MIME_TYPE}' `+
				"and trashed = false",
			spaces: spaces.join(", "),
			fields: "files(id, name, spaces, mimeType), nextPageToken",
			pageSize: getAll ? MAX_PAGE_SIZE : pageSize,

		};
		return gapi.client.drive.files.list(requestParams)
		.then(response => this._processFilesResponse(response, requestParams, getAll));
	}

	// (Called only once per dev-initiated request)
	_processFilesResponse(response, requestParams, getAll) {
		if (getAll) {
			// Load any remaining files
			return this._listRemainingFiles(response, requestParams)
				.then(files => ({ files }) );
		} else {
			// Return a data object
			const { files, nextPageToken } = response.result;
			const returnData = { files };
			if (nextPageToken) {
				returnData.loadMore = () => this._listPageFiles(nextPageToken, requestParams);
			}
			return returnData;
		}
	}

	// Lists a single page of files
	_listPageFiles(pageToken, requestParams) {
		requestParams.pageToken = pageToken;
		return gapi.client.drive.files.list(requestParams)
		.then(response => this._processFilesResponse(response, requestParams, false));
	}

	// A recursive function to get all pages of a files.list request
	_listRemainingFiles(response, requestParams) {
		const files = response.result.files;
		const nextPageToken = response.result.nextPageToken;
		if (nextPageToken) {
			requestParams.pageToken = nextPageToken;
			return gapi.client.drive.files.list(requestParams)
			.then(response => this._listRemainingFiles(response, requestParams))
			.then(moreFiles => files.concat(moreFiles));
		} else {
			return Promise.resolve(files);
		}
	}

	getOrCreateFile(parentId, spaces, name, folder=false) {
		return gapi.client.drive.files.list({
			q:
				`name='${name}' `+
				`and '${parentId}' in parents `+
				`and mimeType ${folder?"=":"!="} '${FOLDER_MIME_TYPE}' `+
				"and trashed = false",
			spaces: spaces.join(", "),
			fields: "files(id, spaces, mimeType)",

		}).then(response => { // Query completed
			if (!response || !response.result || !response.result.files)
				throw new Error("No results given");

			const files = response.result.files;
			if (!files.length) {
				// Make new file/folder
				return this._createFile(parentId, name, folder);
			}
			// Return file/folder data
			const {id, spaces, mimeType} = files[0];
			if (files.length !== 1) {
				console.warn(`Found more than one "${name}" in "${parentId}" folder. Returning first one (ID: ${id})`);
			}
			return {id, name, spaces, mimeType};
		});
	}

	_createFile(parentId, name, folder=false) {
		return gapi.client.drive.files.create({
			resource: {
				name: name,
				parents: [parentId],
				mimeType: folder ? FOLDER_MIME_TYPE : undefined,
			},
			fields: "id, spaces",
		}).then(response => {
			const {id, spaces} = response.result;
			return { id, name, spaces };
		});
	}

	//======================================================================
	//			Used by File class

	readFileContents(id, opts) {
		return gapi.client.drive.files.get({
			fileId: id,
			alt: "media",
		}).then(response => {
			if (response) return opts.raw ? response.body : response.result;
			else return null;
		});
	}

	writeFileContents(id, contents) {
		return gapi.client.request({
			path: "/upload/drive/v3/files/" + id,
			method: "PATCH",
			params: { uploadType: "media" },
			body: contents,
		});
	}

}
