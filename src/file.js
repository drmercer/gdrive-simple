export default class File {
	constructor(props, utils) {
		Object.assign(this, props);
		this.utils = utils;
	}

	read(opts) {
		return this.utils.network
			.readFileContents(this.id, opts);
	}

	write(data) {
		return this.utils.network
			.writeFileContents(this.id, data)
			.then(() => ({})); // Return an empty object on success (for extensibility)
	}
}
