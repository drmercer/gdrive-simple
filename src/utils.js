import NetworkUtils from "./networkutils.js";

export default class Utils {
	constructor(File, Folder) {
		this.File = File;
		this.Folder = Folder;
		this.network = new NetworkUtils();
	}

	inflateData(data, isFolder=false) {
		return isFolder ? new this.Folder(data, this) : new this.File(data, this);
	}
}
