import loadJS from "load-js";

// Cache the loading Promise so we only load the Google API script once:
var gapiLoadPromise = null;

const params = {
	url: "https://apis.google.com/js/api.js",
	cache: false,
};

export default function load() {
	if (!gapiLoadPromise) {
		gapiLoadPromise = loadJS(params)
			.then(
				() => {},
				err => {
					// Clear the gapiLoadPromise so we can try again later
					gapiLoadPromise = null;
					throw err;
				});
	}
	return gapiLoadPromise;
}

// module.exports = load;
