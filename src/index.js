
import gapiLoad from "./gapi-loader.js";
import BaseFolder from "./folder.js";
import BaseFile from "./file.js";
import Utils from "./utils.js";
import NetworkUtils from "./networkutils.js";
import safePromise from "./safepromise.js";

// Start loading the Google API. Do nothing if it fails - the dev can handle
// that with a .catch() when they call init().
gapiLoad().catch(() => console.warn("An error occurred preloading the Google API"));

/* global gapi */// (tells ESLint that gapi is a global)

const DEFAULT_DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

// This object contains everything but File- or Folder-creating functions. Those
// are created per-instance in the exported function down below, so that users
// can extend the Folder and/or File classes as they see fit.
const gds = {};

function loadClient() {
	return gapiLoad()
		.then(function() {
			if (!gapi.client) {
				return new Promise(resolve => gapi.load("client:auth2", resolve));
			}
		});
}

//======================================================================
//			Initialization and authentication

/**
 * Initializes the Google API client. See
 * @param  {Object} options
 *         		An options hash containing the `clientId` to use, a `scopes` array,
 *         		and an optional `discoveryDocs` array (defaults to
 *         		`"https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"`).
 * @return {Promise}
 *         		A Promise that resolves when the client has been initialized.
 */
gds.init = function({ clientId, scopes, discoveryDocs=DEFAULT_DISCOVERY_DOCS }) {
	return safePromise(() => loadClient().then(() => {
		return gapi.client.init({
			discoveryDocs,
			clientId,
			scope: scopes.join(" "),
		});
	}));
};

//======================================================================
//			Signed-in state handling functions:

/**
 * Calls the given function with the current signed-in state (after the current
 * call stack clears), and then calls that function again whenever the signed-in
 * state changes.
 * @param  {Function} cb
 *         The listener function. Receives a boolean indicating whether the user
 *         is signed in.
 */
gds.listenForSignInChange = cb => {
	const auth = gapi.auth2.getAuthInstance();
	auth.isSignedIn.listen(cb);
	setTimeout(() => cb(auth.isSignedIn.get()), 0);
};

/**
 * @return {Boolean} true if the user is currently signed in, false if not.
 */
gds.isUserSignedIn = () => gapi.auth2.getAuthInstance().isSignedIn.get();
/**
 * Signs in the user using the Google API. https://goo.gl/37MKyM
 */
gds.signIn = opts => gapi.auth2.getAuthInstance().signIn(opts);
/**
 * Signs out the user using the Google API. https://goo.gl/sC22Np
 */
gds.signOut = () => gapi.auth2.getAuthInstance().signOut();

/**
 * Returns the current user's BasicProfile. https://goo.gl/r8GtH6
 * @return {gapi.auth2.BasicProfile} The current user's BasicProfile.
 */
gds.getUserProfile = () => gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();

//======================================================================
//			Factory with per-instance methods (any that instantiate Folders or Files)

const gdsFactory = (File=BaseFile, Folder=BaseFolder, utils) => {
	const instance = Object.assign({}, gds);

	if (!utils) utils = new Utils(File, Folder);

	/**
	 * Returns a Folder representing the root of the user's Drive.
	 * @return {Folder} The root Folder.
	 */
	instance.getRootFolder = function() {
		return new Folder({
			id: "root",
			name: "Drive",
			spaces: ["drive"],
		}, utils);
	};

	/**
	 * Returns a Folder representing your app's private App Data folder.
	 * @return {Folder} The appDataFolder
	 */
	instance.getAppDataFolder = function() {
		return new Folder({
			id: "appDataFolder",
			name: "App Data",
			spaces: ["appDataFolder"],
		}, utils);
	};

	return instance;
};

//======================================================================
//			Export a basic instance, the factory, and the base classes

module.exports = gdsFactory(BaseFile, BaseFolder);
module.exports.gdsFactory = gdsFactory;
module.exports.Folder = BaseFolder;
module.exports.File = BaseFile;
module.exports.Utils = Utils;
module.exports.NetworkUtils = NetworkUtils;
