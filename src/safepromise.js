/**
 * A simple wrapper that catches errors thrown when starting a promise.
 */
export default function(fn) {
	return Promise.resolve().then(fn);
}
