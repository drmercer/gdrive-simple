
import {gdsFactory, File, Folder} from "./index.js";

class MyFile extends File {
	constructor(...args) {
		super(...args);
	}
}

class MyFolder extends Folder {
	constructor(...args) {
		super(...args);
	}
}

const gds = gdsFactory(MyFile, MyFolder);

gds.init({
	clientId: "262230741042-6pk925ik2nrmpkp7ekb35r149p1om5t9.apps.googleusercontent.com",
	scopes: [
		"https://www.googleapis.com/auth/drive.appdata",
		"https://www.googleapis.com/auth/drive",
	],
})
.then(function() {
	console.log("gds ready");
	gds.listenForSignInChange(updateSignedInStatus);
});

function updateSignedInStatus(signedIn) {
	console.info(signedIn ? "Signed in" : "Not signed in");
	const btn = document.getElementById("signIn");
	const content = document.getElementById("content");

	if (!signedIn) {

		btn.onclick = gds.signIn;
		btn.innerText = "Sign In";
		content.innerText = "";

	} else {
		const userProfile = gds.getUserProfile();
		content.innerText = `Signed in as ${userProfile.getName()} (${userProfile.getEmail()})`;

		btn.onclick = gds.signOut;
		btn.innerText = "Sign Out";

		// Get the user's Drive root:
		const root = gds.getRootFolder();
		// Get your app's private App Data folder:
		const appdata = gds.getAppDataFolder();

		// List files in a folder:
		root.listFiles()
			.then(function processResult(result) {
				const files = result.files;
				if (result.loadMore) {
					return result.loadMore()
						.then(processResult)
						.then(moreFiles => files.concat(moreFiles));
				} else {
					return files;
				}
			})
			.then(files => {
				console.log("Files in the user's Drive:", files);
			});

		// Get a file by name (creating it if it doesn't exist), and
		// write some JSON to it:
		appdata.getFile("myfile.json")
			.then(file => file.write( {thisIsSome:"json"} ))
			.then(() => console.log("myfile.json succesfully written."));

		// Get a subdirectory, then get a file inside it:
		appdata.getFolder("myfolder")
			.then(folder => folder.getFile("myfileinmyfolder.json"))
			.then(file => file.read())
			.then(contents => console.log("Contents:", contents));
	}
}
