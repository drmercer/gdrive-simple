/**
 * Represents a folder in the Google Drive filesystem
 */
export default class Folder {
	/**
	 * Constructs a new Folder object.
	 * @param  {Object} props
	 *         The properties of this Folder, such as id, spaces, name, etc.
	 * @param  {Utils} utils
	 *         The Utils object to use for inflating File and Folder objects
	 */
	constructor(props, utils) {
		Object.assign(this, props);
		this.utils = utils;
	}

	/**
	 * Returns a Promise for a list of files that are direct children of this folder.
	 * @param  {Boolean} [folders]
	 *         		(optional) Whether to list folders instead of (non-folder)
	 *         		files. Defaults to false.
	 * @return {Promise<File[]>} A Promise for a list of files.
	 */
	listFiles({folders=false, getAll=false, pageSize}={}) {
		const inflateData =
			data => this.utils.inflateData(data, folders);

		return this.utils.network
			.listFolderChildren(this.id, this.spaces, {folders, getAll, pageSize})
			.then(function processResult(result) {
				// Be sure to call inflateData for each file data object returned:
				const files = result.files.map(inflateData);
				if (result.loadMore) {
					const loadMore = () => result.loadMore().then(processResult);
					return { files, loadMore };
				} else {
					return { files };
				}
			});
	}

	/*
	 * Equivalent to listFiles(options) with options.folders = true
	 */
	listFolders(options={}) {
		options.folders = true;
		return this.listFiles(options);
	}

	getFile(name, {folder=false}={}) {
		return this.utils.network
			.getOrCreateFile(this.id, this.spaces, name, folder)
			.then(data =>
				this.utils.inflateData(data, folder)
			);
	}

	getFolder(name, options={}) {
		options.folder = true;
		return this.getFile(name, options);
	}
}
