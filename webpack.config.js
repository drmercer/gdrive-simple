/* eslint-disable */

module.exports = {
	entry: {
		"index": "./src/index.js",
		"sample": "./src/sample.js"
	},
	output: {
		filename: "./dist/[name].bundle.js",
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env']
					},
				},
			},
		],
	},
	devtool: "source-map",
};
