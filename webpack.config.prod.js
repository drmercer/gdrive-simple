/* eslint-disable */

var mainConfig = require("./webpack.config.js");

var nodeExternals = require("webpack-node-externals");

module.exports = Object.assign(mainConfig, {
	entry: {
		"index": __dirname + "/src/index.js"
	},
	output: {
		filename: "./dist/[name].bundle.js",
		library: "gdrive-simple",
		libraryTarget: "umd",
		umdNamedDefine: true,
	},
	externals: [
		nodeExternals(),
	],
});
